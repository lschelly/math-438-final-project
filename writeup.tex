\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[retainorgcmds]{IEEEtrantools}
\usepackage{comment}
\usepackage{cancel}
\usepackage{listings}
\usepackage[colorlinks=true,urlcolor=blue]{hyperref}
\usepackage{verbatim}
\usepackage[margin=1.4in]{geometry}

\newcommand{\norm}[1]{\left\lVert#1\right\rVert}
\newcommand{\Reals}{\mathbb{R}}
\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Python stuff
%Credit: 
%https://tex.stackexchange.com/questions/83882/how-to-highlight-python-syntax-in-latex-listings-lstinputlistings-command
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Default fixed font does not support bold face
\DeclareFixedFont{\ttb}{T1}{txtt}{bx}{n}{10} % for bold
\DeclareFixedFont{\ttm}{T1}{txtt}{m}{n}{10}  % for normal

% Custom colors
\usepackage{color}
\definecolor{deepblue}{rgb}{0,0,0.5}
\definecolor{deepred}{rgb}{0.6,0,0}
\definecolor{deepgreen}{rgb}{0,0.5,0}

% Python style for highlighting
\newcommand\pythonstyle{\lstset{
      language=Python,
      backgroundcolor=\color[RGB]{240,240,240},
      tabsize=4,
      basewidth=.5em,
      rulecolor=\color{black},
      basicstyle=\normalsize\ttfamily,    % code text size and font
      %upquote=true,
      columns=fixed,
      extendedchars=true,
      breaklines=true,
      prebreak = \raisebox{0ex}[0ex][0ex]{\ensuremath{\hookleftarrow}},
      frame=lrtb,
      xleftmargin=5pt,
      xrightmargin=5pt,
      framesep=4pt,
      framerule=2pt,
      numberstyle=\tiny,
      numbers=left,
      showtabs=false,
      showspaces=false,
      showstringspaces=false,
      keywordstyle=\color[RGB]{42, 161, 152},   %\color[RGB]{42, 161, 152},
      commentstyle=\color[RGB]{108, 153, 8},   %\color[RGB]{108, 153, 8},
      stringstyle=\color[RGB]{189, 78, 98},     %\color[RGB]{189, 78, 98},
      title=\lstname,
      captionpos=b,
    %  abovecaptionskip=-5pt,
    %  belowcaptionskip=-5pt,
      moredelim=[is][\color{black}]{<<}{>>},
      moredelim=[is][\color{red}]{<r<}{>r>},
      moredelim=[is][\color{blue}]{<b<}{>b>},
      moredelim=[is][\color{green}]{<g<}{>g>},
      morekeywords={assert, self, super, with, as, True, False, BaseException, Exception, AssertionError, AttributeError, ImportError, IndexError, KeyError, KeyboardInterrupt, MemoryError, NameError, NotImplementedError, OSError, OverflowError, RecursionError, RuntimeError, StopIteration, SyntaxError, IndentationError, TabError, StandardError, SystemError, SystemExit, TypeError, ValueError, ZeroDivisionError, IOError, Warning, RuntimeWarning}
}}

% Python environment
\lstnewenvironment{python}[1][]
{
\pythonstyle
\lstset{#1}
}
{}

% Python for external files
\newcommand\pythonexternal[2][]{{
\pythonstyle
\lstinputlisting[#1]{#2}}}

% Python for inline
\newcommand\pythoninline[1]{{\pythonstyle\lstinline!#1!}}



\title{Math 438 Final Project Writeup}
\author{Logan Schelly}
\date{April 2019}

\begin{document}

\maketitle

\section{Inverted Pendulum: Equations of Motion}
In this class we studied the physics of an inverted pendulum.
Specifically, we considered what happens when you take a ball, stick it to the end of a pole, and then attach the pole to a cart.  
You constrain the cart to only be able to move back and forth in a straight line. 
You also constrain the pole to only be able to pivot back and forth along the straight line, like it's attached to the cart by a hinge or an axle.
We also allow a time-varying force to act on the cart to move it back and forth.

\begin{wrapfigure}{r}{0.25\textwidth} %this figure will be at the right
    \caption{Picture of a cart-pendulum system stolen from Wikipedia.}
    \centering
    \includegraphics[width=0.25\textwidth]{InvertedPendulumFromWikipedia}
\end{wrapfigure}

To describe how the cart, rod, and ball will move, I will create some variables:
\begin{itemize}
    \item $m_c$ -- The mass of the cart.
    \item $m_b$ -- The mass of the ball.
    \item $\ell$ -- The length of the pole.
    \item $\delta:[0, \ell] \mapsto \Reals^{\geq 0}$ -- The linear density of the pole, as a function of distance from the pivot point.
    \item $x$ -- The rightward position of the cart.
    \item $\theta$ -- The rod's counter-clockwise angle from vertical.
    \item $F$ -- The rightward force acting on the cart.
\end{itemize}

Given these values, we know the x-y coordinate of the cart is $(x, 0)^T$.  
If a point on the rod is a distance $r$ from the pivot point, then you can use trigonometry to find that its coordinate is $(x-r\sin(\theta), r\cos(\theta))^T$.

From this, we can discover the potential energy of the system:
\begin{IEEEeqnarray*}{rCl}
    U(x, \theta) &=& \overbrace{\int_{0}^{\ell}r\cos(\theta)g\delta(r)dr}^{\text{rod's potential energy.}}
    +\overbrace{\ell \cos(\theta) g m_b}^{\text{ball's}} - \overbrace{Fx}^{\text{cart's `potential energy'}}
    \\
    &=& g\left( \int_{0}^{\ell}\delta(r)r dr +  m_b \ell\right)\cos(\theta) - Fx
\end{IEEEeqnarray*}

Let $\Dot{x}$ represent the velocity of the cart, and let $\Dot{\theta}$ represent the angular velocity of the pole.
We know that the x-y velocity of the cart is then $(\Dot{x}, 0)^T$.  By the chain rule, we know that a point on the pole at distance $r$ from the pivot will have x-y velocity of $(\Dot{x} - r\cos(\theta)\Dot{\theta}, -r\sin(\theta)\Dot{\theta})^T$.

From this, we can find the total kinetic energy of the system:
\begin{IEEEeqnarray*}{rCl?s}
    T &=& \overbrace{\frac{1}{2}m_c (\Dot{x})^2}^{\text{cart}} 
    + \overbrace{
        \int_{0}^{\ell} \frac{1}{2}\delta(r)
        \norm{\begin{bmatrix}\Dot{x} - r\cos(\theta)\Dot{\theta}\\ -r\sin(\theta)\Dot{\theta} \end{bmatrix}}^2 dr
            }^{\text{pole}}
    +\overbrace{\frac{1}{2}m_b
        \norm{\begin{bmatrix}\Dot{x} - \ell\cos(\theta)\Dot{\theta}\\ -\ell\sin(\theta)\Dot{\theta} \end{bmatrix}}^2}
        ^{\text{ball}}
    \\
    &=&
    \frac{1}{2}
    \begin{pmatrix}
    \left(m_c + \int_{0}^{\ell}\delta(r)dr + m_b \right)\Dot{x}^2
        - 2\left(\int_{0}^{\ell}\delta(r)rdr + m_b\ell \right)\cos(\theta)\Dot{\theta}\Dot{x}
        \\
        +\left(\int_{0}^{\ell} \delta(r) r^2 dr + m_b\ell^2\right)\Dot{\theta}^2
        \end{pmatrix}
\end{IEEEeqnarray*}

Let $I_p = \int_{0}^{\ell} \delta(r) r^2 dr$ be the moment of inertia for the pole, 
and let $I_b = m_b\ell^2$ be the moment of inertia of the ball.  Let $M_p = \int_{0}^{\ell} \delta(r)r dr$
represent the first moment of area of the pole.  
Let $M_b = m_b \ell$ be the first moment of area of the ball.
Finally, let $m_p = \int_{0}^{\ell}\delta(r)dr$ be the total mass of the pole.
If we substitute this into the expressions for $U$ and $T$ we get
\begin{IEEEeqnarray*}{rCl}
    U(x, \theta) &=& g\left( M_p + M_b\right)\cos(\theta) - Fx
    \\
    T(x, \theta, \Dot{x}, \Dot{\theta}) &=& \left(m_c + m_p + m_b \right)\frac{\Dot{x}^2}{2} - \left(M_p + M_b\right)\cos(\theta)\Dot{\theta}\Dot{x}
            +(I_p + I_b)\frac{\Dot{\theta}^2}{2}
\end{IEEEeqnarray*}
\begin{comment}
With the goal of applying Hamilton's principle, we will define the Lagrangian of this system.  It will map from the state of the system to the difference between kinetic and potential energy:
\begin{equation*}
    L(x, \theta, \Dot{x}, \Dot{\theta}) = T(x, \theta, \Dot{x}, \Dot{\theta}) - U(x, \theta)
\end{equation*}

If we consider $x$ and $\theta$ as functions of time, Hamilton's principle states
\begin{equation*}
    x, \theta = \argmin_{y, \phi \in C^2([t_0, t_1], \Reals)}
    \int_{t_0}^{t_1}L(y(t), \phi(t), y'(t), \phi'(t))dt.
\end{equation*}
\end{comment}

If we treat $x$ and $\theta$ as functions of time rather than just real numbers, Hamilton's principle says that
\begin{equation*}
    x, \theta = \argmin_{y, \phi \in C^2([t_0, t_1], \Reals)}
    \int_{t_0}^{t_1}T(y(t), \phi(t), y'(t), \phi'(t)) - U(y(t), \phi(t))dt.
\end{equation*}

\noindent By the Euler-Lagrange equations for $x$ we get
\begin{IEEEeqnarray*}{c}
    \frac{\partial}{\partial x}(T-U) - \frac{d}{dt} \frac{\partial }{\partial \Dot{x}} (T - U) = 0\\
    0 - (-F) - \frac{d}{dt}\left((m_c + m_p + m_b)\Dot{x} - (M_p + M_b)\cos(\theta) \Dot{\theta} -0 \right) = 0\\
    F = (m_c + m_p + m_b)\Ddot{x} + (M_p + M_b)\left(\sin(\theta)\dot{\theta}^2-\cos(\theta)\ddot{\theta}\right)
    \\
    F - (M_p + M_b)\sin(\theta)\dot{\theta}^2
    = (m_c + m_p + m_b)\Ddot{x} - (M_p + M_b)\cos(\theta)\ddot{\theta}
    .\yesnumber \label{eqn: Euler-Lagrange for x}
\end{IEEEeqnarray*}

\noindent Similarly for $\theta$,
\begin{IEEEeqnarray*}{c}
    \frac{\partial}{\partial \theta}(T-U) - \frac{d}{dt} \frac{\partial }{\partial \Dot{\theta}} (T - U) = 0\\
    (M_p + M_b)\sin(\theta)\Dot{\theta}\Dot{x}+ g(M_p + M_b)\sin(\theta) 
    -\frac{d}{dt}\left( - (M_p + M_b)\cos(\theta)\Dot{x} + (I_b + I_p)\Dot{\theta} - 0 \right) = 0\\
    \cancel{(M_p + M_b)\sin(\theta)\dot{\theta}\dot{x}} +  g(M_p + M_b)\sin(\theta) 
    = \cancel{(M_p + M_b)\sin(\theta)\dot{\theta}\dot{x}}
     -(M_p + M_b)\cos(\theta)\Ddot{x} + (I_p + I_b)\ddot{\theta}\\
     g(M_p + M_b)\sin(\theta) 
    = -(M_p + M_b)\cos(\theta)\Ddot{x} + (I_p + I_b)\ddot{\theta} \yesnumber \label{eqn: Euler-Lagrange for theta}
\end{IEEEeqnarray*}

\noindent If we combine equation \eqref{eqn: Euler-Lagrange for x} with equation \eqref{eqn: Euler-Lagrange for theta}
into matrix-vector form, we have
\begin{equation*}
    \begin{bmatrix}
        m_c + m_p + m_b & -(M_p + M_b) \cos(\theta)\\
        -(M_p + M_b) \cos(\theta) & I_p + I_b
    \end{bmatrix}
    \begin{bmatrix} \ddot{x} \\ \ddot{\theta} \end{bmatrix}
    =
    \begin{bmatrix}
    F - (M_p + M_b)\sin(\theta)\dot{\theta}^2\\
     g(M_p + M_b)\sin(\theta) 
    \end{bmatrix}
\end{equation*}
Let $I_{tot} = I_p + I_b$, $M_{tot} = M_p + M_b,$ and $m_{tot} = m_c + m_p + m_b$.
Then multiply by the matrix inverse on both sides to get
\begin{IEEEeqnarray}{rCl}
    \ddot{x} &=& 
        \frac{I_{tot}\left(F - M_{tot}\sin(\theta)\dot{\theta}^2\right) + 
            gM_{tot}^2\cos(\theta)\sin(\theta)}
        {I_{tot}m_{tot} - M_{tot}^2 \cos^2(\theta)}
        \label{eqn: Equation of motion for x}
    \\
    \ddot{\theta} &=& 
        \frac{M_{tot}\cos(\theta)F
            - M_{tot}^2 \cos(\theta)\sin(\theta)(\dot{\theta})^2
            + gm_{tot}M_{tot}\sin(\theta)}
        {I_{tot}m_{tot} - M_{tot}^2 \cos^2(\theta)}.
        \label{eqn: Equation of motion for theta}
\end{IEEEeqnarray}

\begin{comment}
Then, turn this into a first order system of four equations:
\begin{equation}
    \begin{bmatrix}
    x\\
    \theta\\
    \dot{x}\\
    \dot{\theta}
    \end{bmatrix}'
    =
    \begin{bmatrix}
    \dot{x}\\
    \dot{\theta}\\
    \frac{I_{tot}F - g M_{tot}^2 \cos(\theta)\sin(\theta)}{I_{tot} m_{tot} - M_{tot}^2 \cos^2(\theta)}\\
    \rule{0ex}{4ex}
    \frac{-M_{tot}\cos(\theta)F + gm_{tot} M_{tot}\sin(\theta)}{I_{tot} m_{tot} - M_{tot}^2 \cos^2(\theta)}\\
    \end{bmatrix}
    \label{eqn: First-Order Equation of Motion}
\end{equation}
\end{comment}

\subsection{Massless Pole}
Wikipedia and our class resources treat the pole as massless ($\delta = 0$).
This leads to $I_p = M_p = m_p = 0$.
Therefore, $I_{tot} = I_b = m_b \ell^2$, and $M_{tot} = m_b \ell$, and $m_{tot} = m_c + m_b$.
Substituting this into the equations of motion we get
\begin{IEEEeqnarray*}{rCl}
    \ddot{x} &=& \frac{m_b \ell^2 \left(F -m_b \ell \sin(\theta)\dot{\theta}^2 \right)+ g m_b^2 \ell^2\cos(\theta)\sin(\theta)}
    {m_b\ell^2(m_c+m_b) - m_b^2 \ell^2 \cos^2(\theta)}
    \\
    &=&
    \frac{F  -m_b \ell \sin(\theta)\dot{\theta}^2 +g m_b \cos(\theta)\sin(\theta)}
    {m_c + m_b\sin^2(\theta)}
    \\
    \ddot{\theta} &=&
    \frac{m_b\ell \cos(\theta) F 
        - m_b^2 \ell^2 \cos(\theta)\sin(\theta)\dot{\theta}^2
        + g(m_c + m_b)m_b\ell\sin(\theta)}
    {m_b\ell^2(m_c+m_b) - m_b^2 \ell^2 \cos^2(\theta)}
    \\
    &=&
    \frac{\cos(\theta) F 
        - m_b \ell \cos(\theta)\sin(\theta)\dot{\theta}^2
        + g(m_c + m_b)\sin(\theta)}
    {\ell(m_c+m_b\sin^2(\theta))}.
\end{IEEEeqnarray*}

\section{Simulating the Cart/Pole System}
I decided to make a simulator for the inverted pendulum problem.
It turns out OpenAI's \texttt{gym} python library already has a \texttt{cartPoleEnv}
class.  However, there were a couple adjustments I needed to make

\subsection{Differences in OpenAI Implementation}
The source code for the class is available at
\url{https://github.com/openai/gym/blob/master/gym/envs/classic_control/cartpole.py}.
OpenAI has a few crucial differences in how it models the cart and pole environment.
Specifically, OpenAI distributes the mass differently, measures the pole angle differently, and
measures the pole length differently.

First, let's look at the equations of motion in the \texttt{cartPoleEnv} class:
\pythonexternal[firstnumber=98, firstline=98, lastline=100]{cartpole.py}

To understand some of the class variables, it's worth seeing how they are 
initialized in the constructor:

\pythonexternal[firstnumber=56, firstline=56, lastline=63]{cartpole.py}

Strangely, the \texttt{length} variable used in the equations of motion is actually \emph{half} the pole length!
It's also worth noting that OpenAI only uses the mass of the pole, and no
ball is assumed to be attached to the end.

I later found out that the \texttt{theta} in OpenAI's code means something different than our $\theta$.
For that reason, I will notate the OpenAI variable as $\vartheta$.
I substituted the variable \texttt{temp} into
lines 99 and 100, used $\frac{\ell}{2}$ for \texttt{length}, and simplified:
\begin{IEEEeqnarray}{rCl}
    \ddot{\vartheta} &=&
    \frac{(m_c + m_p)g\sin(\vartheta) - F\cos(\vartheta) - \frac{m_p \ell}{2} \dot{\vartheta}^2 \sin(\vartheta)\cos(\vartheta)}
    {\frac{\ell}{2}\left(\tfrac{4}{3}(m_c + m_p) - m_p\cos^2(\vartheta)\right)}
    \label{eqn: OpenAI Equation of Motion for theta}
    \\
    \ddot{x} &=& \frac{F + \frac{m_p \ell}{2} \dot{\vartheta}^2 \sin(\vartheta) - \frac{m_p \ell}{2} \ddot{\vartheta}\cos(\vartheta)}{m_c + m_p}
    \label{eqn: OpenAI Equation of Motion for x}
\end{IEEEeqnarray}

Where do these equations even come from?
Well, if we treat the pole as though it has uniform density, then we have
$\delta(r) = \frac{m_p}{\ell}$.
We can then use this to find the first moment of area and the moment of inertia of the pole:
\begin{IEEEeqnarray*}{rCl}
M_p &=& \int_{0}^{\ell}\delta(r)rdr = \frac{m_p}{\ell} \frac{r^2}{2}\Big|_{0}^{\ell} = \frac{m_p \ell}{2}\\
I_p &=& \int_{0}^{\ell}\delta(r)r^2dr = \frac{m_p}{\ell} \frac{r^3}{3}\Big|_{0}^{\ell} = \frac{m_p \ell^2}{3}
\end{IEEEeqnarray*}

Looking at the variable names in the class, it doesn't look like OpenAI is simulating a ball at the end of the rod.  So, we can say that $m_b = I_b = M_b = 0$.
This means $M_{tot} = M_p = \frac{m_p \ell}{2}$ and $I_{tot} = I_p$.
Also, the total mass is just $m_{tot} = m_c + m_p$.
If we substitute this into equation \eqref{eqn: Equation of motion for theta}
we get
\begin{IEEEeqnarray*}{rCl?s}
    \ddot{\theta} &=& 
    \frac{\frac{m_p \ell}{2}\cos(\theta)F
            - \frac{m_p^2 \ell^2}{4} \cos(\theta)\sin(\theta)\dot{\theta}^2
            + g(m_c + m_p)\frac{m_p \ell}{2}\sin(\theta)}
        {\frac{m_p \ell^2}{3}(m_c + m_p) -\frac{m_p^2 \ell^2}{4} \cos^2(\theta)}
        &Substitute.
    \\
    &=&
    \frac{\cos(\theta)F
            - \frac{m_p \ell}{2} \cos(\theta)\sin(\theta)\dot{\theta}^2
            + g(m_c + m_p)\sin(\theta)}
        {\frac{2\ell}{3}(m_c + m_p) -\frac{m_p \ell}{2} \cos^2(\theta)}
        &Multiply by $\frac{2}{m_p \ell}$.
    \\
    &=&
    \frac{(m_c + m_p)g\sin(\theta)
            + \cos(\theta)F
            - \frac{m_p \ell}{2} \dot{\theta}^2\cos(\theta)\sin(\theta)}
        {\frac{\ell}{2}\left(\frac{4}{3}(m_c + m_p) -m_p \cos^2(\theta)\right)}.
    &Rearrange.
\end{IEEEeqnarray*}
Furthermore, if we solve equation \eqref{eqn: Euler-Lagrange for x} for $\ddot{x}$
we get
\begin{IEEEeqnarray*}{rCl?s}
    \ddot{x} &=& 
    \frac{F - (M_p + M_b)\sin(\theta)\dot{\theta}^2
        +(M_p + M_b)\cos(\theta)\ddot{\theta}}
    {m_c + m_p + m_b}
    &Solve for $\ddot{x}$.
    \\
    &=&
    \frac{F - \frac{m_p \ell}{2}\sin(\theta)\dot{\theta}^2 
        + \frac{m_p \ell}{2}\cos(\theta)\ddot{\theta}}
    {m_c + m_p} &Subsitute known values.
    \\
    &=&
    \frac{F - \frac{m_p \ell}{2}\dot{\theta}^2 \sin(\theta)
        + \frac{m_p \ell}{2}\ddot{\theta}\cos(\theta)}
    {m_c + m_p} &Rearrange.
\end{IEEEeqnarray*}

These equations for $\ddot{x}$ and $\ddot{\theta}$ seem almost identical to the
equations of motion \eqref{eqn: OpenAI Equation of Motion for theta} 
and \eqref{eqn: OpenAI Equation of Motion for x} for OpenAI.
However, if you look closely, you'll see a disturbing number of sign changes.

Where did the sign changes come from?
To answer that, I had to follow quite the trail of citations.
The top of the \texttt{cartpole.py} documentation says that 
the code is copied from \url{http://incompleteideas.net/sutton/book/code/pole.c}.
I followed the link to the c-file, and \emph{that} file had a citation to 
Richard Sutton's 1984 PhD dissertation, ``Temporal
Aspects of Credit Assignment in Reinforcement Learning".
I was able to find a copy of the dissertation at
\url{https://search.proquest.com/docview/303321395}.
Sutton introduces the cart-pole system late in his dissertation, along with this picture on page 183:

\begin{center}
    \includegraphics[width=0.6\textwidth]{InvertedPendulumBySutton}
\end{center}

As you can see, Sutton measured the pole angle in the clockwise direction!  

If you make the substitution $\vartheta = -\theta$
in equations \eqref{eqn: OpenAI Equation of Motion for theta} 
and \eqref{eqn: OpenAI Equation of Motion for x},
the sign changes go away.

To summarize:\begin{itemize}
    \item OpenAI's equations are for a rod of uniform density with no ball attached to the end.
    \item What OpenAI calls \texttt{length} is what we call $\frac{\ell}{2}$.
    \item OpenAI measures the pole angle in the clockwise direction.
\end{itemize}
While it was kind of tough to track down these differences, it's a relief to know that the differences are only from the notation and modifications to the system.  In the end, OpenAI agrees with how the physics should work. 

\subsection{Modifying the Code}

\subsubsection*{Sign Changes}

I first rewrote the code so that $\texttt{theta}$ was counterclockwise rotation.  
At first I thought I would only need to change the equations of motion.
However, once I ran a couple test runs, I saw that the cart and pole were not behaving at
all how I would expect them to.
Eventually, I realized that I needed to change the \texttt{render()} function as well.
Originally, it rendered as if the angle were clockwise.

\subsubsection*{Numerical Method}

OpenAI uses Euler's method to update the state.
I changed the numerical method to be Runge-Kutta.
To make the code more readable, I wrote a method \texttt{get\_state\_derivative()}.
In the end, here's how the Runge-Kutta implementation ended up looking:\vspace{4ex}

\pythonexternal[firstnumber=119, firstline=119, lastline=125]{mycartpole.py}

While using Runge-Kutta might seem like overkill, I found it gave a much more realistic simulation.  Here's one example:  I simulated 1,500 time steps of $\tfrac{1}{50}$ of a second each.  I had the pole start a few degrees away from straight down.  I had no forces acting on the system, aside from gravity.  When I used OpenAI's method, the pole would swing back and forth, and gain height with each swing.  Eventually, it started rotating all the way around.
However, when I used my method, the pole's swings did not get larger.

\subsubsection*{Pole}
When I started out this project, I wanted to make it so that I could simulate a pole of
non-uniform density.
As I worked, I realized that the only relevant characteristics of the pole would be the mass,
the length, the first moment of area, and the moment of inertia.
So, I made a class called \texttt{Pole} that would store those attributes.
I toyed with the idea of making it possible for someone to pass in a vector or a function that would represent the linear density of the pole, but I decided it would be best to first get done with the rest of the project.
In the meantime, you can still simulate a pole of arbitrary linear density.
You just need to calculate the mass, first moment of area, and the moment of inertia.

\subsubsection*{Further Customizations}
The original OpenAI simulation only allowed the control to be full force in the positive or negative direction.  I made it so that the control can also be any force in between.

I also updated the constructor of the class so that you can customize the mass of the cart, the kind of pole attached to the cart, the maximum force that can be applied to the cart, whether or not a ball is attached to the end of the pole, and the mass of the ball.

Finally, I added a bit of pizzazz to the visualization of the cart/pole system: If there was a ball on the end of the pole, I made the render method draw one.

\section{Implementing Optimal Control}

I first tried implementing a Linear Quadratic Regulator.
I also tried using a control scheme that did not linearize the system, and instead used Pontryagin's maximum principle.


\subsection{Linear Quadratic Regulator (LQR)}

One of the first control schemes I tried to use was a linear quadratic regulator (LQR).
The first step is to linearize the system around where $\theta = 0$ and $\dot{\theta} = 0$.
This allows you to make the approximations $\sin(\theta) \approx \theta$, $\cos(\theta) \approx 1$,
and $\dot{\theta}^2 \approx 0$.  Plugging these approximations into equations \eqref{eqn: Equation of motion for x}
and \eqref{eqn: Equation of motion for theta} you get

\begin{equation*}
        \ddot{x} = 
        \frac{I_{tot}F + gM_{tot}^2\theta}
        {I_{tot}m_{tot} - M_{tot}^2}
    \quad
    \text{ and }
    \quad
    \ddot{\theta} =
        \frac{M_{tot}F
            + gm_{tot}M_{tot}\theta}
        {I_{tot}m_{tot} - M_{tot}^2}.
\end{equation*}

\noindent
After you make the change of variables $x = x_1$, $\dot{x} = x_2$, $\theta = \theta_1$, and $\dot{\theta} = \theta_2$,
you can then turn this into a linear system of four first order differential equations:

\begin{equation*}
    \begin{bmatrix}x_1\\ x_2\\ \theta_1\\ \theta_2\end{bmatrix}'
    =
    \underbrace{
        \begin{bmatrix}
        0&1&0&0\\
        0&0&\frac{gM_{tot}^2}{I_{tot} m_{tot} - M_{tot}^2} & 0\\
        0&0&0&1\\
        0&0&\frac{gm_{tot}M_{tot}}{I_{tot} m_{tot} - M_{tot}^2}&0
        \end{bmatrix}
    }_{A}
    \begin{bmatrix} x_1\\ x_2\\\theta_1\\ \theta_2 \end{bmatrix}
    +
    \underbrace{
        \begin{bmatrix}
        0\\ 
        \frac{I_{tot}}{I_{tot} m_{tot} - M_{tot}^2}\\
        0\\
        \frac{M_{tot}}{I_{tot} m_{tot} - M_{tot}^2}
        \end{bmatrix}
    }_{B}
    \begin{bmatrix}F\end{bmatrix}.
\end{equation*}

Second, to apply LQR, you also need a specific functional to optimize over.
If we let $\mathbf{x} = \begin{bmatrix}x_1, x_2, \theta_1, \theta_2 \end{bmatrix}$, 
then one good functional is
\begin{equation*}
    J[F] = \int_{0}^{\infty} \mathbf{x}^T Q \mathbf{x} + r F^2 dt
\end{equation*}
where $Q \in M_{4\times 4}(\Reals)$ is positive definite, and $r \in\Reals^{+}$.
Our textbook explains that for this specific functional the optimal control is
\begin{equation}
    F  = \frac{1}{r} B^T P \mathbf{x}.
    \label{eqn: control for LQR}
\end{equation}
Here, $P$ is the solution to the algebraic Ricatti Equation:
\begin{equation*}
    PA + A^T P + Q - PB r^{-1} B^T P = 0.
\end{equation*}

One extremely convenient aspect of this control is that it does not require calculation of a costate vector,
we can simply calculate the control directly from the state.

\subsubsection*{Implementing LQR}

Implementing a LQR was surprisingly easy.
To find $P$, I used the function  \texttt{solve\_continuous\_are()} from \texttt{scipy.linalg}.
Then, I just took the state and used equation \eqref{eqn: control for LQR} to calculate the 
LQR control.

Even more surprising was how well the LQR was able to work.
I thought it would fail terribly if I perturbed the pendulum too far from equilibrium.
Instead, it was actually able to return the pendulum to the upright position.

I think the reason why LQR succeeded is because I had coded to state vector so that $\theta \in [-\pi, \pi]$.
I noticed that when the pendulum would swing beneath the bottom of the cart, the direction of the force would
completely change signs.  This makes some sense, because that's where $\theta$ would switch from $\pi$ to $-\pi$.
This force switch would make the pendulum gradually swing higher and higher each time, like a kid on a swing.
After several swings, the LQR was able to get the cart underneath the pendulum, and then balance it.
I was pleasantly surprised.

Once the pendulum was near upright, the LQR did a very good job at keeping it upright.

\subsection{Pontryagin's Maximum Principle}
After seeing LQR succeed, it kind of took the wind out of my sails for trying other methods.
I had supposed that I would need more than LQR to deal with when the pendulum was very far away from upright.

Nonetheless, I formulated a cost functional, to see if I could come up with a control that could swing up from straight down
in a 5 second period.
So, I first imposed the condition of $(x(0), \dot{x}(0), \theta(0), \dot{\theta}(0)) = (0, 0, 0, 0)$.
I also needed the force to be bounded : $|F| \leq F_{max}$.
I needed to severely penalize if the pendulum was not upright at the end of 5 seconds, so I included the term
$10^4 \theta(5)^2$ in the functional.
I also needed to penalize any movement that could be happening at that time, so I included the terms
$10^2\dot{x}(5)^2$ and $10^2\dot{\theta}(5)^2$.
Finally, I didn't want the cart moving too far from the origin, so I also included $\int_{0}^{5}x(t)^2 dt + 10^2 x(5)^2$.
The final functional was

\begin{equation*}
    J[F] = \int_{0}^{5}x(t)^2dt +10^2 x(5)^2 + 10^2\dot{x}(5)^2+ 10^2\dot{\theta}(5)^2 + 10^4\theta(5)^2.
\end{equation*}

If we drop the subscripts from $I_{tot}$, $M_{tot}$, and $m_{tot}$, the Hamiltonian of this problem is 
\begin{IEEEeqnarray*}{rCl}
    H &=& \mathbf{p} \cdot \mathbf{f} - L
    \\
    &=&
    p_1x_2 + p_2 \left(\frac{I(F - M\sin(\theta_1)\theta_2^2) + gM^2 \cos(\theta_1)\sin(\theta_1) }{Im - M^2 \cos(\theta_1)^2} \right)
    \\
    &&
    p_3\theta_2 + p_4 \left(
        \frac{M\cos(\theta_1)F
            - M^2 \cos(\theta_1)\sin(\theta_1)\theta_2^2
            + gmM\sin(\theta_1)}
        {Im - M^2 \cos^2(\theta_1)}\right)
        -x_1^2
\end{IEEEeqnarray*}
Where $\mathbf{p} = (p_1, p_2, p_3, p_4)$ is the costate vector.  By Pontryagin's maximum principle, the optimal value for F
should maximize the Hamiltonian. In our case, the Hamiltonian is linear in F
\begin{equation*}
    \frac{\partial H}{\partial F} = 
    \frac{p_2 I + p_4 M \cos(\theta_1)}{I m - M^2 \cos(\theta_1)^2}
\end{equation*}

That gives us the following control:
\begin{equation*}
    \Tilde{H} = \text{sign} \left( \frac{p_2 I + p_4 M \cos(\theta_1)}{I m - M^2\cos(\theta_1)^2}\right)
    \cdot F_{max}
\end{equation*}

Of course, this means that we need to find out the costate, $\mathbf{p}$.
We have learned that the optimal costate statisfies $\mathbf{p}' = \frac{\partial H}{\partial \mathbf{x}}$.  Therefore,

\begin{IEEEeqnarray*}{rCl}
p_1' &=& -2x_1\\ 
p_2' &=& p_1\\
p_3' &=&\frac{
    p_2 \left(-IM\cos(\theta_1)\theta_2^2 + gM^2 (\cos^2(\theta_1) - \sin^2(\theta_1)) \right)
    (Im - M^2 \cos^2(\theta_1))}
    {(Im - M^2 \cos^2(\theta_1))^2}
    \\
    &&-\frac{p_2\left(I(F - M\sin(\theta_1)\theta_2^2) + gM^2 \cos(\theta_1)\sin(\theta_1)\right)
    (2M^2\cos(\theta_1)\sin(\theta_1))}
    {(Im - M^2 \cos^2(\theta_1))^2}
    \\
    &&+\frac{p_4\left(-M \sin(\theta_1)F - M^2\left(\cos^2(\theta_1) - \sin(\theta_1)^2\right) \theta_2^2 \right)(Im - M^2 \cos^2(\theta_1))}{(Im - M^2 \cos^2(\theta_1))^2}\\
    &&-\frac{p_4\left(M\cos(\theta_1)F- M^2 \cos(\theta_1)\sin(\theta_1)\theta_2^2+ gmM\sin(\theta_1)\right)
    (2M^2\cos(\theta_1)\sin(\theta_1))}
    {(Im - M^2 \cos^2(\theta_1))^2}
    \\
p_4' &=& p_2\left(\frac{-2M\sin(\theta_1)\theta_2}{Im - M^2\cos(\theta_1)^2}\right)
        +p_4\left(\frac{-2M^2\cos(\theta_1)\sin(\theta_1)\theta_2}{Im - M^2 \cos(\theta_1)^2} \right)
\end{IEEEeqnarray*}
We also know that $\mathbf{p}(5) = -\frac{\partial \phi}{\partial\mathbf{x}}(5)$.  Therefore,

\begin{IEEEeqnarray*}{rCl}
p_1(5) &=& 200 x_1(5)\\
p_2(5) &=& 200 x_2(5)\\
p_3(5) &=& 2\cdot10^4 \cdot \theta_1(5)\\
p_4(5) &=& 200 \theta_2(5)
\end{IEEEeqnarray*}

I didn't really know a way to solve that system of equations by hand, so instead I just tried to iteratively solve the system as follows:

\begin{enumerate}
    \item Make a rudimentary guess at the optimal control.
    \item Use a numerical solver to approximate how the state would evolve from that control.
    \item Use the final state value to now run a numerical solver through backwards time to see how the costate would evolve.
    \item Construct a better guess for the optimal control using the state and costate
    from steps 2 and 3.
    \item Repeat from step 2.
\end{enumerate}

This was the method that we used in our HIV lab, and I hoped that it would work well in
this problem.  However, I found the results to be very disappointing.

It took a very long amount of time for the computer to iterate to a solution.
Then, when I tested the solution, I found that it didn't even swing up the pole.

While it was disappointing, it does give me a deeper appreciation for how powerful the LQR method is.


\end{document}
