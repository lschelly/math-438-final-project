import numpy as np
from mycartpole import ContinuousPoleCart, Pole

p = Pole(2.0, 0.1)

env = ContinuousPoleCart(ball_mass = .1, pole=p)
env.state = np.array([0, 0, .1, 0])

for i in range(300):
    env.step(np.zeros(1))
    env.render()
    
env.close()
