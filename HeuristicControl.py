import numpy as np
from mycartpole import ContinuousPoleCart, Pole
from scipy import linalg as la

#Make an instance of a cartpole system.
p = Pole(4., 0.0)
cart_mass = 23.
ball_mass = 5.
env = ContinuousPoleCart(pole=p, cart_mass=cart_mass, 
                        ball_mass=ball_mass, seconds_per_iteration=.01,
                        max_force = 200.0)

#Start with the pole hanging straight down.
state0 = np.array([0, 0, np.pi, 0])
env.state = state0

#Define the matrices for LQR
g = env.gravity
A = np.array([
    [0, 1, 0,             0],
    [0, 0, ball_mass*g/cart_mass,         0],
    [0, 0, 0,             1],
    [0, 0, g/(cart_mass*p.length)*(cart_mass+ball_mass), 0]
])
B = np.array([
    [0],
    [1/cart_mass],
    [0],
    [1/(cart_mass * p.length)]])
Q = np.diag([1., 1., 1., 1.])
R = np.array([[1.]])

#Solve the algebraic Ricatti equation.
P = la.solve_continuous_are(A, B, Q, R)


env.initializeViewer(world_width=30)

doing_LQR = False
going_left = True
old_angle = env.state[2]
env.step(np.array([1]))

for i in range(1000):
	#Look at the current configuration
	new_angle = env.state[2]
	position = env.state[0]
	doing_LQR = abs(new_angle) < .1
	
	if not doing_LQR:
		#determine which direction we want to go.
		if going_left:
			if (new_angle < old_angle) or (position < -5):
				going_left = False
	
		else:
			if (new_angle > old_angle) or position > 5:
				going_left = True
	
		if going_left:
			action = np.array([-1])
		else:
			action = np.array([1])
			
	else:
		print("Doing LQR")
		u = - la.solve(R, B.T.dot(P.dot(env.state)))
		u_mag = np.abs(u)
		u_sign = np.sign(u)
		action = np.min([1., u_mag/env.max_force])
		action *= u_sign
	
	env.step(action)
	env.render()
	old_angle = new_angle

env.close()
