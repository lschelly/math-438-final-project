import numpy as np
from scipy.integrate import solve_ivp
from mycartpole import Pole, ContinuousPoleCart

#Set up the time domain
t, dt = np.linspace(0, 5, 501, retstep=True)

#Make an instance of a cartpole system.
p = Pole(4., 0.0)
cart_mass = 23.
ball_mass = 5.
env = ContinuousPoleCart(pole=p, cart_mass=cart_mass, 
                        ball_mass=ball_mass, seconds_per_iteration=dt,
                        max_force = 200.0)

#Start with the pole hanging straight down.
state0 = np.array([0, 0, np.pi, 0])
env.state = state0

#Define the function that gives the optimal control from the state and costate.
def get_optimal_control(state, costate):

    #Unpack variables.
    x1, x2, theta1, theta2 = state
    p1, p2, p3, p4 = costate
    F_max = env.max_force
    I = env.total_moment_of_inertia
    m = env.total_mass
    M = env.total_first_moment_of_area
    
    #Return the optimal control.
    return np.sign((p2*I + p4*M*np.cos(theta1))
                /(I*m - M**2 * np.cos(theta1)**2))*F_max


#Define a function that gives the derivative of the costate
def get_costate_derivative(state, costate, control):

    #Unpack variables.
    x1, x2, theta1, theta2 = state
    p1, p2, p3, p4 = costate
    F = control
    I = env.total_moment_of_inertia
    m = env.total_mass
    M = env.total_first_moment_of_area
    g = env.gravity
    
    #Calculate p1 and p2's derivatives.
    p1_dot = -2 * x1
    p2_dot = p1
    
    #Get p3's derivative.  Need to use the quotient rule.
    low = I * m - M**2 * np.cos(theta1)**2
    dlow = 2 * M**2 * np.cos(theta1) * np.sin(theta1)
    
    high1 = I * (F - M*np.sin(theta1)*theta2**2)
    high1 += g*M**2 * np.cos(theta1) * np.sin(theta1)
    
    dhigh1 = -I*M*np.cos(theta1) * theta2**2 
    dhigh1 += g*M**2 *(np.cos(theta1)**2 - np.sin(theta1)**2)
    
    high2 = M*np.cos(theta1)*F 
    high2 -= M**2 * np.cos(theta1)*np.sin(theta1) * theta2**2
    high2 += g*m*M*np.sin(theta1)
    
    dhigh2 = -M*np.sin(theta1)*F
    dhigh2 -= M**2 * (np.cos(theta1)**2 - np.sin(theta1)**2) * theta2**2
    dhigh2 += g*m*M*np.cos(theta1)
    
    p3_dot = p2*(low*dhigh1 - high1*dlow) + p4*(low*dhigh2 - high2*dlow)
    p3_dot /= low**2
    
    #Get p4's derivative.
    p4_dot = p2*(-2*M*np.sin(theta1)*theta2)
    p4_dot += p4*(-2*M**2*np.cos(theta1)*np.sin(theta1)*theta2)
    
    return np.array([p1_dot, p2_dot, p3_dot, p4_dot])
    
    

#Make a guess about the costate/optimal control.
optimal_control = np.zeros(t.size)
costate = np.zeros((4, t.size))

#Allocate memory for the state
state = np.empty((4, t.size))

#Wrapper function that uses the time to find the optimal control at that time.
def state_deriv(time, state):
	index = t.searchsorted(time, side='left')
	u = optimal_control[index]
	return env.get_state_derivative(state, u)

#Wrapper function that uses the time to find the state and optimal control at that time.
def costate_deriv(time, costate):
	i = t.searchsorted(time, side='left')
	return get_costate_derivative(state[:,i], costate, optimal_control[i])

#Iteratively approximate optimal control.
for _ in range(100):
	
	print("Iteration", _ , end='\r')
	
	#Evolve the state equations forward using that new control.
	old_state = state
	state = solve_ivp(state_deriv, (t[0], t[-1]), state0, t_eval=t)['y']
	
	#Use the final end state to determine the end conditions for the costate.
	old_costate = costate
	costate_end = state[:, -1]*np.array([200, 200, 20000, 200])
	costate_backwards = solve_ivp(costate_deriv, (t[-1], t[0]),
	                         costate_end, t_eval=t[::-1])['y']
	costate = costate_backwards[:, ::-1]
	
	#Get the optimal control from the current state and costate guesses.
	optimal_control = get_optimal_control(state, costate)
	
	if np.allclose(state, old_state) and np.allclose(costate, old_costate):
	    print("Breaking on iteration", _)
	    break

	
	
print("Optimal control:", optimal_control)

#Display the trajectory.
env.initializeViewer(world_width=110,screen_width=1620, screen_height=600)
for u in optimal_control:
	env.step(np.array([u])/env.max_force)
	env.render()

#Save what we found.
information = np.vstack([optimal_control, state, costate])
np.save('PontryaginStateEvolution', information)

env.close()



