"""
Cart/pole inverted pendulum system.

Heavily inspired by the CartPoleEnv from open ai's CartPoleEnv

The open ai version came from an implementation by Rich Sutton et al.
at http://incompleteideas.net/sutton/book/code/pole.c
permalink: https://perma.cc/C9ZM-652R

The implementation by Richard Sutton is based on his PhD thesis,
"Temporal Credit Assignment In Reinforcement Learning" page 182.
last available at https://search.proquest.com/docview/303321395
"""

import math
from math import sin, cos
import gym
from gym import spaces, logger
from gym.utils import seeding
import numpy as np
from gym.envs.classic_control import rendering

class Pole():
    
    def __init__(self, length, mass, first_moment_of_area=None, 
                    moment_of_inertia=None):
        
        self.length = length
        self.mass = mass
        
        #Don't allow the first moment of area to be passed without the
        #moment of inertia.
        assert (first_moment_of_area is None) == (moment_of_inertia is None)
        
        if (first_moment_of_area is None):
            #Assume uniform density
            self.first_moment_of_area = self.mass * self.length / 2
            self.moment_of_inertia = self.mass * self.length**2 / 3
            
        else:
            self.first_moment_of_area = first_moment_of_area
            self.moment_of_inertia = moment_of_inertia


class ContinuousPoleCart(gym.Env):

    def __init__(self, gravity=9.8, cart_mass=1.0, pole=Pole(1.0, 0.1), 
            ball_mass=0.0, max_force=10.0, seconds_per_iteration=0.02):
        
        #Store the meaningful parameters.
        self.gravity = gravity #gravitational acceleration.
        self.cart_mass = cart_mass
        self.pole = pole
        self.ball_mass = ball_mass
        self.max_force = max_force
        self.seconds_per_iteration = seconds_per_iteration
        
        #Calculate the quantities we'll need repeatedly.
        self.ball_first_moment_of_area = ball_mass * pole.length
        self.ball_moment_of_inertia = ball_mass * pole.length**2
        self.total_mass = (cart_mass + pole.mass + ball_mass)
        
        self.total_first_moment_of_area = \
                self.ball_first_moment_of_area + self.pole.first_moment_of_area
                
        self.first_moment_squared = self.total_first_moment_of_area**2
        
        self.total_moment_of_inertia = \
                self.ball_moment_of_inertia + self.pole.moment_of_inertia
        
        self.moment_of_inertia_times_mass = \
                self.total_mass * self.total_moment_of_inertia
        
        #Set the bounds for the action space.
        high = np.ones(1)
        self.action_space = spaces.Box(-high, high, dtype=np.float64)
        self.viewer = None
    
    #The dynamics of the system.
    def get_state_derivative(self, state, force):
    
        #unpack the variables involved.
        x, x_vel, angle, angle_vel = state
        F = force
        g = self.gravity
        m = self.total_mass
        M = self.total_first_moment_of_area
        M_sq = self.first_moment_squared
        I = self.total_moment_of_inertia
        I_times_m = self.moment_of_inertia_times_mass
        
        #To save time, do these operations only once:
        sine, cosine = sin(angle), cos(angle)
        sine_times_cosine = sine*cosine
        angle_vel_sq = angle_vel**2
        denominator = I_times_m - M_sq * cosine**2
        
        #These are the actual equations of motion.
        x_acc = I * (F - M * sine * angle_vel_sq) 
        x_acc += M_sq * sine_times_cosine * g
        x_acc /= denominator
        angle_acc = M * cosine * F - M_sq * sine_times_cosine * angle_vel_sq
        angle_acc += m * g * M * sine
        angle_acc /= denominator 
        
        return np.hstack([x_vel, x_acc, angle_vel, angle_acc])
        
    #Run one step of Runge-Kutta to evolve the system.
    def step(self, action):
    
        #Check to make sure it's a valid action.
        assert self.action_space.contains(action), "%r (%s) invalid"%(action, type(action))
        
        #Unpack the state variables.
        state = self.state
        force = self.max_force * action
        
        #Run the update using Runge-Kutta:
        h = self.seconds_per_iteration #step size in Runge-Kutta.
        k0 = self.get_state_derivative(state, force)
        k1 = self.get_state_derivative(state + (h/2) * k0, force)
        k2 = self.get_state_derivative(state + (h/2) * k1, force)
        k3 = self.get_state_derivative(state + h * k2, force)
        approx_average_derivative = (k0 + 2 * k1 + 2* k2 + k3)/6
        self.state += approx_average_derivative * h
        
        #Make sure the angle is within acceptable bounds.
        angle = self.state[2]
        if (angle >= np.pi) or (angle <= np.pi):
            normalized = ((angle + np.pi)%(2*np.pi))-np.pi
            self.state[2] = normalized
        
        return
        
    
    def initializeViewer(self, world_width=5, screen_width=600, screen_height=400):
        
        scale = screen_width/world_width
        self.scale = scale
        carty = screen_height/3 #Top of the cart.
        pole_width = 5/60 * scale
        cart_width = 25/60 * scale
        cart_height = 5/20 * scale
        there_is_a_ball = self.ball_mass > 0.0
        if there_is_a_ball:
            ball_radius = pole_width
        
        #Create the viewer.
        self.viewer = rendering.Viewer(screen_width, screen_height)
        
        #Put the track the cart is on.
        track = rendering.Line((0,carty), (screen_width,carty))
        track.set_color(0,0,0)
        self.viewer.add_geom(track)
        
        #Start making everything for the cart.
        right, top = cart_width/2, cart_height/2
        left, bottom = -right, -top
        cart = rendering.FilledPolygon([(left, bottom), (left, top), 
                                        (right, top), (right, bottom)])
        self.cart_transformation = rendering.Transform()
        cart.add_attr(self.cart_transformation)
        self.viewer.add_geom(cart)
        
        #Make the pole and the axle
        right = pole_width/2
        top = self.pole.length*scale - pole_width/2
        left = bottom = -right
        pole_polygon = rendering.FilledPolygon([(left, bottom), (left, top)
                                                ,(right, top), (right, bottom)])
        pole_polygon.set_color(.8, .6, .4)
        axleoffset = cart_height / 4
        self.pole_transformation = rendering.Transform(
                                translation=(0, axleoffset))
        pole_polygon.add_attr(self.pole_transformation)
        pole_polygon.add_attr(self.cart_transformation)
        self.viewer.add_geom(pole_polygon)
        axle_circle = rendering.make_circle(pole_width/2)
        axle_circle.add_attr(self.pole_transformation)
        axle_circle.add_attr(self.cart_transformation)
        axle_circle.set_color(.5, .5, .8)
        self.viewer.add_geom(axle_circle)
        
        #Make the ball at the end of the pole.
        if there_is_a_ball:
            ball = rendering.make_circle(ball_radius)
            ball.set_color(.5, .5, .8)
            ball_transformation = rendering.Transform(
                    translation=(0, self.pole.length*scale))
            ball.add_attr(ball_transformation)
            ball.add_attr(self.pole_transformation)
            ball.add_attr(self.cart_transformation)
            self.viewer.add_geom(ball) 
        
        return
        
      
    def render(self, mode='human'):

        if self.viewer is None:
            self.initializeViewer()

        if self.state is None: return None

        x = self.state
        cartx = x[0]*self.scale+self.viewer.width/2.0 # MIDDLE OF CART
        carty = self.viewer.height/3
        self.cart_transformation.set_translation(cartx, carty)
        self.pole_transformation.set_rotation(x[2])

        return self.viewer.render(return_rgb_array = mode=='rgb_array')
    
    def close(self):
        if self.viewer:
            self.viewer.close()
            self.viewer = None
