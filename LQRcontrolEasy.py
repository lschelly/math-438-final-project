import numpy as np
from mycartpole import ContinuousPoleCart, Pole
from scipy import linalg as la


#Set up everything the same way we did in the lab.
p = Pole(4., 0.0)
cart_mass = 23.
ball_mass = 5.
env = ContinuousPoleCart(pole=p, cart_mass=cart_mass, 
                        ball_mass=ball_mass, seconds_per_iteration=.01,
                        max_force = 200.0)

#Define the matrices for LQR
g = env.gravity
A = np.array([
    [0, 1, 0,             0],
    [0, 0, ball_mass*g/cart_mass,         0],
    [0, 0, 0,             1],
    [0, 0, g/(cart_mass*p.length)*(cart_mass+ball_mass), 0]
])
B = np.array([
    [0],
    [1/cart_mass],
    [0],
    [1/(cart_mass * p.length)]])
Q = np.diag([1., 1., 1., 1.])
R = np.array([[1.]])

#Solve the algebraic Ricatti equation.
P = la.solve_continuous_are(A, B, Q, R)

begin_state = np.array([0, 0, .1, 0])
env.state = begin_state
env.initializeViewer(world_width=20, screen_width=1000, screen_height=600)
env.render()
input("Press enter to start the simulation.")

for i in range(3000):
    u = - la.solve(R, B.T.dot(P.dot(env.state)))
    u_mag = np.abs(u)
    u_sign = np.sign(u)
    action = np.min([1., u_mag/env.max_force])
    action *= u_sign
    env.step(action)
    #print("This is u:", u)
    #print("This was the action", action)
    env.render()
    #print()
    
env.close()
