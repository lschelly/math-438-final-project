import numpy as np
from mycartpole import ContinuousPoleCart
import time

original_state = np.array([0, 0, 3., 0])

#Run the simulation with my environment, which uses Runge-Kutta.
print("Running my pole cart environment.")
env = ContinuousPoleCart()
env.state = original_state.copy()
tot_step_time = 0.0
tot_render_time = 0.0
for i in range(1500):
    t = time.time()
    env.step(np.zeros(1))
    tot_step_time += time.time() - t
    t = time.time() 
    env.render()
    tot_render_time += time.time() - t
print("Total step time:{0}, {1}/%".format(tot_step_time, 100* tot_step_time /(tot_render_time + tot_step_time)))
print("Total render time:{0}, {1}/%".format( tot_render_time, 100 * tot_render_time / (tot_render_time + tot_step_time)))
env.close()

from cartpole_corrupted import CartPoleEnv
input()
print("Running a modified version of the OpenAI environment.")
env = CartPoleEnv()
env.state = - original_state.copy()
tot_step_time = 0.0
tot_render_time = 0.0
for i in range(1500):
    t = time.time()
    env.step(0)
    tot_step_time += time.time() - t
    t = time.time() 
    env.render()
    tot_render_time += time.time() - t
print("Total step time:{0}, {1}/%".format(tot_step_time, 100* tot_step_time /(tot_render_time + tot_step_time)))
print("Total render time:{0}, {1}/%".format( tot_render_time, 100 * tot_render_time / (tot_render_time + tot_step_time)))
env.close()
